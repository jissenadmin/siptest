<?php
class DAO extends PDO{
    const PARAM_host = 'localhost';
    const PARAM_dbname = 'sipusers';
    const PARAM_user = 'sipper';
    const PARAM_pass = 'mypass';

    public function __construct(){
        try {
            parent::__construct(
                'mysql:host='.DAO::PARAM_host.
                ';dbname='.DAO::PARAM_dbname, DAO::PARAM_user, DAO::PARAM_pass);
        } catch (PDOException $e) {
            print $e->getMessage();
            die();
        }
    }

    public function query($query){
        $args = func_get_args();
        array_shift($args);
        $reponse = parent::prepare($query);
        if ($reponse->execute($args)) {
            while ($row = $reponse->fetch()) {
                return $row;
            }
        } else {
            return $reponse;
        }
    }

    public function DBKill(){
        $dbh = null;
        $stmt = null;
    }

}