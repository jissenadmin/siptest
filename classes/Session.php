<?php
class Session {
    public function require_unlogined_session () {
        @session_start();
        if (isset($_SESSION["isLogin"])) {
            header('Location: ./sip.php');
            exit;
        }
    }

    public function require_logined_session() {
        @session_start();
        if (!isset($_SESSION["isLogin"])) {
            header('Location: ./login.php');
            exit;
        }
    }

    public function require_logout_session() {
        @session_start();
        unset($_SESSION["isLogin"]);
        header('Location: ./login.php');
        exit;
    }
}