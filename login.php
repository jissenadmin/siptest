<?php
require_once './classes/GetData.php';
require_once './classes/Session.php';

$s = new Session();
$s->require_unlogined_session();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $g = new GetData();
    $id = $_POST['userName'];
    $pass = $_POST['userPassword'];

    $errors = "";

    if ( $id == "" || $pass == "" ) {
        $errors = "ユーザ名またはパスワードが入力されていません。";
    } else {
        $data = $g->setter($id,$pass);
        if($data === "true"){
            session_regenerate_id(true);
            $_SESSION['isLogin'] = $id;
            header ('Location: ./sip.php?id='.$id);
            exit;
        }
        $errors = "ユーザ名またはパスワードが違います";
    }
}
header ('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIPテスト - ログイン</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Roboto:400);
body {
  background-color:#fff;
  -webkit-font-smoothing: antialiased;
  font: normal 14px Roboto,arial,sans-serif;
}

#errmsg {
	color: red;
}

.container {
    padding: 25px;
    position: fixed;
}

.form-login {
    background-color: #EDEDED;
    padding-top: 10px;
    padding-bottom: 20px;
    padding-left: 20px;
    padding-right: 20px;
    border-radius: 15px;
    border-color:#d2d2d2;
    border-width: 5px;
    box-shadow:0 1px 0 #cfcfcf;
}

h4 {
 border:0 solid #fff;
 border-bottom-width:1px;
 padding-bottom:10px;
 text-align: center;
}

.form-control {
    border-radius: 10px;
}

.wrapper {
    text-align: center;
}
</style>
</head>
<body>
<div class="container" style="max-width:100%">
    <div class="row justify-content-md-center">
        <div class="col-md-5">
            <div class="form-login">
            <h4>SIP通話テスト</h4>
            <?php if($errors != ""){ echo '<p id="errmsg">IDまたはパスワードが間違っています</p>' ;}?>
            <form action="login.php" method="POST">
            	<input type="text" name="userName" id="userName" class="form-control input-sm chat-input" placeholder="ユーザID" /><br/>
            	<input type="password" name="userPassword" id="userPassword" class="form-control input-sm chat-input" placeholder="パスワード" /><br/>
            </form>
            <div class="wrapper">
            <span class="group-btn">
                <button id="submit" class="btn btn-primary btn-md">ログイン <i class="fa fa-sign-in"></i></button>
            </span>
            </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<script type="text/javascript">
$("#submit").click(function() {
	$('form').submit();
})
</script>
</body>
</html>