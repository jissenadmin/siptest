<?php
require_once './classes/GetData.php';
require_once './classes/Session.php';
$s = new Session();
$s->require_logined_session();

$userlist = array("hisamitsu", "marketing", "ehisa", "eitahisamitsu");
//$id = $_SESSION['isLogin'];
$id = $_GET['id'];

$g = new GetData();
$data = $g->getter($id);
foreach($data as $key => $value){
    $d[$key] = $value;
}

header ('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIPテスト - 通話画面</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
<link href="css/sip.css" rel="stylesheet" type="text/css">
<style type="text/css">
.animationload {
    background-color: #fff;
    height: 100%;
    left: 0;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 10000;
}
.osahanloading {
    animation: 1.5s linear 0s normal none infinite running osahanloading;
    background: #fed37f none repeat scroll 0 0;
    border-radius: 50px;
    height: 50px;
    left: 50%;
    margin-left: -25px;
    margin-top: -25px;
    position: absolute;
    top: 50%;
    width: 50px;
}
.osahanloading::after {
    animation: 1.5s linear 0s normal none infinite running osahanloading_after;
    border-color: #85d6de transparent;
    border-radius: 80px;
    border-style: solid;
    border-width: 10px;
    content: "";
    height: 80px;
    left: -15px;
    position: absolute;
    top: -15px;
    width: 80px;
}
@keyframes osahanloading {
0% {
    transform: rotate(0deg);
}
50% {
    background: #85d6de none repeat scroll 0 0;
    transform: rotate(180deg);
}
100% {
    transform: rotate(360deg);
}
}
</style>
<script src="js/sip.js" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<a href="logout.php">ログアウト</a>
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
          <div class="sms-send-page">
            <ul class="list-group">
            <?php
                foreach($userlist as $list){
                    if($list === $id){continue;}
            ?>
              <li class="list-group-item">
                <div class="col-xs-8 pull-left">
                  <img class="img-circle img-rounded img-responsive" src="http://www.webncc.in/images/gurdeeposahan.jpg" alt="iamgurdeeposahan">
                  <p><strong><?php echo $list; ?></strong></p>
                  <small><i class="fa fa-circle text-success" aria-hidden="true"></i> Online</small>
                </div>
                <div class="col-xs-4 pull-right">
                  <button id="callbutton" type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" data-val="<?php echo $list; ?>">電話する <i class="fa fa-phone" aria-hidden="true"></i></button>
                </div>
                <div class="clearfix"></div>
              </li>
            <?php }?>
            </ul>
          </div>
        </div>
    </div>

  <!-- Outbound Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style="width:100%">
      <!--
        <div class="modal-header" style="border-bottom:none">
          <button id="endCall2" type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
      -->
        <div class="modal-body">
        	<div id="loading" class="animationload" style="position:initial;">
        		<div class="osahanloading"></div>
        	</div>
        	<video id="remoteVideo" style="display:block;margin:0 auto;width:90%"></video>
        </div>
        <div class="modal-footer" style="border-top:none">
        	<button id="endCall" type="button" class="btn btn-danger" data-dismiss="modal">電話を切る</button>
        </div>
      </div>

    </div>
  </div>

</div>
<script type="text/javascript">
var remotevideo = document.getElementById('remoteVideo');
var loadicon = document.getElementById('loading');

var simple = new SIP.WebRTC.Simple({
	media: {
		remote: {
			video: remotevideo,
			audio: remotevideo
		}
	},
	ua: {
		uri: '<?php echo $id; ?>@jissencloudteam.onsip.com',
		authorizationUser: '<?php echo $d["auth"]; ?>',
		password: '<?php echo $d["sippass"]; ?>'
	}
});

$("#callbutton").click(function () {
	var sipname = $(this).attr("data-val");
	simple.call(sipname + '@jissencloudteam.onsip.com');
});

$("#endCall").click(function () {
	simple.hangup();
	loadicon.style.display = 'inherit';
	$('#myModal').modal('hide');
});

simple.on('ringing', function() {
	$('#myModal').modal();
	simple.answer();
});

simple.on('connected', function() {
	loadicon.style.display = 'none';
});

simple.on('ended', function() {
	loadicon.style.display = 'inherit';
	$('#myModal').modal('hide');
});

</script>
</body>
</html>